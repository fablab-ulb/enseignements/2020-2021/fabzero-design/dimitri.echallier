BHello! Je suis Dimitri ECHALLIER

Je suis un étudiant en Erasmus en provenance de Genève. 

Je créais ce site pour vous parler de mes projets, je suis un étudiant en Architetcure et  j'aime bien bricoler

## My background
J'ai fais mes études en France puis l'architecture à Genève à l'HEPIA
Et je fais mon master à l'Hes-so, un ecole qui réunit trois  lieu, un à Genève, le second à Fribourg et le troisième Burgdorf


## About me

![](images/avatar-photo.jpg)

Cette image est une photo prise dans mon appartement à bruxelles avec ma scie sauteuse.

Je vie en france en hautes Savoie proche de la frontière suisse.
Je suis arrivé à Bruxelles pour mon Erasmus en septembre et j’ai pour but de rester au moins un ans ici.
Et à coté de mes études, vu que ici je n’ai ni la place ni l’endroit pour bricoler dans mon appartement ,  je me suis lancé le defis de réaliser un stégosaure avec les encombrants de mon quartier.



## Le stégosaure

Le projet à commencé quand je suis venu les premières fois à Bruxelles.
Je me suis rendue compte du nombre de panneaux en bois ou autres déposé dans les rues .

Depuis l'idée mais venue de faire un stégosaure.
Le projet est de créer un squelette de stégosaure de grande dimension, un peu comme les jouets pour enfant mais en plus grand. Le tout avec des encombrants de mon quartier (st Gilles, Ixelles) et de spécifier l’origine de chaque pièces.

**Attention** :

Des fichiers 2D des pièces pour assembler un animal comme celui-ci  sont trouvable sur internet, en format dxd ou dag mais il faut faire attention avec les fichiers gratuits, ils sont souvent mal dessinés ou les encoches sont mal placées.
Donc faite attention, vérifier votre fichier.

Le mieux à faire est de réaliser une première fois en carton votre objet et redessiner les pièces qu’il ne vous plaise pas pour éviter les mauvaises surprises.


En voici des illustrations plus parlante :

![This is another caption2](images/7DE611C33.jpeg)
La vision d'une maquette en carton gris de la bête et à coté la tête en 1.1
![This is another caption2](images/stego1.jpeg)

La dimension de la tête déjà réalisée, en aggloméré plaquée.
![This is another caption](images/sample-photo.jpg)

Les dimensions sont calibrées en fonction de panneaux de maximum 2 mètres.
Et les panneaux seront gravée de l'adresse ou ils auront été trouvé.

![This is another caption](images/123.jpeg)
Voici les pièces et leurs provenances


**Le stégosaure réalisation**

Là on commence à voir les dimensions, voici le corps, toutes les autres pièces sont découpées, ponçées, et plus qu'a faire les encoches et graver les adresses.

![](images/stegoencour.jpg)

[Voici un lien instagram pour la visualisations du stégosaure](https://www.instagram.com/p/CF_4xbjFo8b/)



## Tadow
Voilà le boulot, plus que les gravures, mais là on se rend bien compte du potentiel de la bête

![](images/stegof.jpg)

![](images/stegoff.jpg)












## LAMPE 4025



Pour ce projet final nous devons choisir un objet d’une collection en plastique des années 50’ 70' et en créer une évolution, réinterprétation.

J’ai choisis une lampe, là voici:


![This is another caption](images/lampe_design.jpg)



**4025, OLAF VON BOHR**


**1973, ABS, mixed media **
**Kartell ITA**

**Raisons de mon choix**

J’ai choisis cette objet tout d’abord parce qu’il me plait.
Deuxièmement pour les principes utilisées par l’auteur.
Un objet qui m'a beaucoup questioné, pour sa façon de s'articuler

**Apprendre Comprendre l'objet**

J'ai voulue comprendre le fontionnement de l'objet, sur cette articulation, mais je n'ai pas trouvé d'informations précises.
(Ce qui est une bonne chose, cela m'a permis de m'approprier l'objet encore plus)

Donc je me suit tout d'abord imaginé comment fonctionnais l'objet.
Après j'ai produit des schémas du principe d'articulation ( hypothèse )


Puis dans le '' module 2 '' vous pouver voir l'objet dessiné.

[Module02](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/dimitri.echallier/-/blob/master/docs/modules/module02.md)

Et dans le '' module 3 '' sa réalisation 

[Module03](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/dimitri.echallier/-/blob/master/docs/modules/module03.md)

Et donc à travers ces deux module j'ai réaliser une copie de mon objet à l'echelle 1:1

Voici la réalisation, mais vous en verré plus dans '' le module 3 ''

![](images/lampeok.jpg)

Et dans le '' module 4 '' j'ai éssayé de lui ajouter un accessoir sans grand succès 

[Module04](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/dimitri.echallier/-/blob/master/docs/modules/module04.md)

Voici une representation de l'accessoir.

![](images/casquetteindex.jpg)




