# DECOUPE LASER

- une machine qui descoupe des panneaux (toujours verifier si la matière à couper n'est pas dangerense)

- Créer un fichier '.svg'  des découpe, entaille, gravure à éffectuer ( diferencier les actions par des couleurs sur votre dessin vectoriel)

- Ouvrir le fichier sur le logiciel de la machine (lasersaure), definir la vitesse et la puissance du laser. ( vérifier sur internet par rapport au materiau utilisé et son épaisseur)

- replaçer son fichier sur le plateau pour reduire les chutes (vous pouvez appuyer sur le bouton a coté de 'RUN' avec un symbole de deux flèches et la machine se deplaçera pour effectuer le contour de votre impression sans lançer le laser)

# Important 

Maintenant il faut lancer la machine mais tout un rituel est a effectuer avant.

- Allumer la machine ( bouton rouge )
- Regler la hauteur de la tête du laser avec une cale en fonction de l'épaiseeur du materiau
- Activer le système de refroidissement 
- Ouvrir la vanne d'aire à . haute pression
- Activer les extracteurs d'air 
- Fermer la porte de la machine 
- Verifier sur le logiciel que tout est 'OK'

Et on est partie 

- Après la découpe, verifier que le bac ne soit pas plein de fumée et si c'est le cas patienter le temp necessaire à son extraction



# 2  Recherche

Pour ce projet j'ai eu l'idée de faire une casquette  dédié  à la lampe 4025 que j'étudie.
Pour ce faire j'ai reflechie à un élement simple et possible de concevoire à partir d'élements en 2d
Voici quelques schemas du projet

![](../images/casquetteidee.jpg)


# 3  Dessiner la decoupe

Pour dessiner la decoupe je me suit servis du logiciel de dessin autocad.

Attention ce logiciel fournis des Dxf (peu compatible avec la lasersaure) donc vous aurez à convertir votre fichier par la suite en .svg

Voici le dessin de decoupe avec en noir le traçé à decouper et en rouge le traçer à entailler.

J'ai voulue que cette casquette ce sert à deux points derrière pour la maintenir, et soit clipsée à l'avant sur la grille de la lampe

![](../images/autocad.jpg)


# 4  Réalisation de la pièce 

Pour les reglages de ma découpe j'ai réglé la tête de la machine à 15mm de mon plateau, car le matériau est très fin donc on peu le decouper en mettant à cette distance, à 15mm on impacte avec le laser à la surface du matériaux à son point le plus puissant.

Si votre matériau  fait 4mm d'épaisseur et que vous voulez le couper, regler la tête du laser à 13mm du plateau pour couper en son centre.
Si vous voulez graver rester à la surface de preférence pour être plus pécis soit à 15mm


ET voila mon objet, la decoupe c'est bien éffectué donc je retiendrais ces reglages pour la prochaine fois avec ce matériau.

![](../images/decoupe.jpg)


Assemblage assez simple pour l'instant elle ressemble à mes recherches



![](../images/casquettefinie.jpg)


# 5  Test sur la lampe

Maintenant qu'on à la Lampe, on peu essayer de l'adapter, malheureusement, ayant fait la casquette avant d'avoir la lampe, j'ai du reajuster mes découpes.

De plus mon système d'attache n'est pas fonctionnel, je n'ai pas effectué correctement mes attaches et donc voila le résultat


![](../images/casquetteratee.jpg)



Et Voila la lampe avec la casquette comme elle aurais du tenir initialement.
Pour ce faire j'ai juste glissé une lame de cutter entre la lampe et la casquette, du bricolage bien entendue, juste pour tester.


![](../images/1.jpg)






