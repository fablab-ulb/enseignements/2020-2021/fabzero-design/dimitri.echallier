# MODELISATION 3D

Dans ce module je vais vous parler de la conception de l’objet choisi au musée.
Je vais vous présenter les différentes étapes qui m’ont permis de réaliser mon objet sur Fusion 360.

## 1 Research

J’ai fais plusieurs recherche de l’objet et je n’ai trouvé que des photos donc j’ai d’abord schématisé la lampe sur papier avant de me lancer dans la conception 3D de celle-ci.

Voici l’ébauche de la lampe avec ma supposition sur le principe mécanique.

![](../images/schema.jpg)

Après réflection j’ai réalisé la 3D de l’objet dans les mêmes principes que ces schémas mais en respectant l’original donc j’ai représenté le pied avec la forme original  

![](../images/cul_lampe.jpg)

Comme on le voit sur cette image, à gauche le socle de l'objet et à droite ma réalisation 3D.

On est pas sur une replique parfaite de l'objet mais j'ai éssayer durant la création sur ordinateur de m'en raprocher un maximum.
## 2 Comment créer des filetages

J'ai eu a faire des filletages sur mes pièces donc J’ai regarder un tutoriel d’une vingtaine de minutes sur youtube, il explique bien tout les principes et astuces pour la réalisation avec une imprimante 3D .
Comme par exemple les tolérances a avoir au niveau du filetage en fonction de la machine.
Je vous met le lien.


- [Tuto Filletages](https://www.youtube.com/watch?v=pxneNgbq7Pk&ab_channel=BenTeK-ConceptionetImpression3D)

voici un detail important, après la réalisation d'un filetage pour impréssion 3D, sachant que la tête de la mahine est précise à 0.2mm et que dans le filetage une tolérance est deja appliqué entre le filetage intérieur et exterieur mais pas assez suffisant, je vous conseil de tirer les faces de une de vos filetages (soit 4faces) de 0,1mm pour l'une des deux partie du filetage soit interieur soit exterieur.
Pour choisir entre untérieur et exterieur, j'ai préferé appliquer cette tolérence sur les partis les plus épaisses de l'objet.

![](../images/filetage.jpg)

## 3 Le problème pour la pièce superieur 

Pour réaliser la pièce supérieur je suis passé par le lissage, qui permet de créer un volume en partant d'une geometrie sur un certain plan jusqu'a une autre géométrie sur un autre plan ou non.
Pour ce faire j'ai du créer un certain nombres de rail pour guider le lissage.
Plus vous créé de lissage plus vous pourrez controler le lissage donc je vous conseil d'en créer au moins quatres si vous avey un lissage d'une forme semblable au mien.

![](../images/lissage.jpg)


## 4 Voila la totalité de mes pièces.

Donc comme pour le model original je suis partie sur un diamètre de 7cm pour le cylindre et j'ai éssayé de respercter au mieux la forme de cette pièce.

Pourquoi 7cm car j'ai fait des recherches sur internet et la description de l'objet mis par des revendeurs, indiquait un diamètre de 7cm et une hauteur de 29cm

Sachant que je connais bien autocad3d et que Fusion360 provient de Autodesk, le logiciel à été assez facile a prendre en main pour cette réalisation sauf pour les deux détails énuméré précédément.

Cette image montre les pièces à cotés de l'original objet, une fois vue du dessus et une fois vue du dessous.

![](../images/toutespieces.jpg)


Donc nous sommes maintenant à la fin de la réalisation sur fusion 360, j'ai pris le temps de verifier à deux fois les dimensions de mes filetages,
revérifier que mon objet avais partout des épaisseurs convenables pour une réalisation physique.
Maintenant plus qu'a programer l'impression sur l'imprimante Prusa.

