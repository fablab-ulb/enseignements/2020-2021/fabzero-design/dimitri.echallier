# DOCUMENTATION

Un des objectifs de ce cours est dédié à la documentation.
Il faut être méthodique dans son travail pour pouvoir réaliser une bonne documentation.

Documenter?
Documenter, c'est expliquer de façon méthodique notre travail pour que quelqu'un puisse reproduire la même chose seulement avec ces documentations.

Pour moi, ce module est plus présent dans les autres modules.
 La réalisation de la documentation des autres modules, c'est ça le travail sur l'art de documenter.


## La Création du Site

Pour la création de ce site sur la plateforme du Gitlab je suis passé par le site web directement, je n'ai pas effectuer la clefs SSH mais je suis convaincue que ce serait utile de l'avoir sur du long therme.

Le principe du codage est nouveau pour moi.

Si vous êtes comme moi je vous conseillerais de regarder  comment pour le site par exemple le système md fonction par rapport à l’interface créée, (comprendre le language du codage) puis récupérer les fonctions et les modifier pour les adapter à vos envis, votre fichier... 

Cela dit, la prise en main de GitLab n’est pas très compliqué, il faut juste être méthodique.


## Techniques


Pour réaliser ma documentation sur Git Lab j'ai utilisé des techniques simples les voici :

- **Ecrire du texte :**

Je le fait directement sur le site web en cliquant sur '' Edit '' et j'ecris mon texte en Md ou sinon pour des grands partie de texte je les ecris précedement sur un éditeur de texte.

D'ailleur j'ai un problème avec mon traducteur Google Chrome donc chaque fois que mon texte et ecrivable dans la langue anglaise, celle-ci se traduit comme '' on '' devient '' sur '' ou '' Filetage '' devient '' Filet ''.
Plutot dommage pour les visiteurs de mon site web cela rend mes phrases imcompréhensibles.

Pour le texte si vous souhaité qu'il soit en gras, cela est très simple, il suffit de placer votre texte comme ceci dans le fichier .md,  **votre texte **

Faite attention à ne pas mettre d'espaces entre votre texte et les étoiles sinon il s'affichera comme mon exemple et non en gras.

Pour les titres vous pouvez écrire le texte dans le fichier .md de cette façon # Titre ou comme ceci ## titre .

La différence entre un titre et écrire en gras est que pour les visiteurs de votre site en fonction de vos configurations, ils peuvents avoir la possibilité de descendre à un titre précis d'une page web. Le titre est un bon procédé si votre page web est bien fournis, les titres permettent d'organiser au mieu son site internet.

- **Importer des images :**

Pour importer des images cela est assez simple, il suffit d'importer des images dans le dossier image dédié à cet effet.
Pour ce faire cliqué sur le petit '' + '' et ensuite sur '' téléverser un fichiert ''

Ensuite rentrer cette formule à l'endroit ou vous souhaiter avoir l'image.

 ! [Texte_devant_image](../dossier_ou_se_situe_l_image/nom_de_l_image.jpg

Faire attention à l'espace que prend une image, privilegier les images de faible résolution

- **Ajouter un lien**

Très simple, lorsque l'on edit sur le web un petit icône '' add link '' vous permet de créer un lien à la ligue ou vous vous trouvez

Puis par la suite rentrer l'url dans l'espace donné

[Texte_donnant_acces_au_lien] (lien)


## La racine du site 

Il est important de s'organiser pour faire un jolie site internet et de spécifier sa nature.
La nous allons parler du fichier YAML qui est le fichier de la racine de votre site et de son apparence.

Les fichiers YAML sont des fichiers servant à représenter des données donc c'est le fichier qui va donner l'apparence de votre site et son accesibilité.

Voici un exemple de mon fichier, je me suis servi du fichier précedent et ai ajouter mon nom et le nom de mon site.

De plus j'ai pris le temps de regarder les site internet des autres étudiants pour voir comment ils ont geré l'interface de leur site et j'ai donc appliqué un thème ''lux'' comme on peut le voir à la onzième ligne de l'image si-dessous.


![](../images/racine.jpg)



## Vérification des opérations 

Pour vérifier que vous avez réussi à pousser vos code, soit réussi à enregistrer vos imformations sur le site.
Aller sur votre site, en tant que mainteneur, vous pouvez avoir accès à une colonne d'action à gauche de votre écran.
Dans cette colonne cliquez sur CI/CD puis job ou emplois en français et vous avez accès à vos dernière modification effectuées et leur satut.
Soit en vert c'est tout bon soit en rouge il y a eu un problème.

Là dans mon exemple, on voit que toutes les dernières fois que j'ai poussé un code cela c'est bien passé.

![](../images/passed.jpg)




