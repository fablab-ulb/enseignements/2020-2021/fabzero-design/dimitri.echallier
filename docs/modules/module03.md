#  IMPRESSION 3D


Pour imprimer les pièces présentées dans le module deux, je me suis servis des imprimantes 3d du FabLab.
Les Prusa MK3s.
Un model de machine connue et pratique car elle est utilisé par toute une communauté qui documente beaucoup à son sujet donc si vous avez un problème vous trouverez généralement la solution sur la toile.
 

Ces machines imprimes sur la base d’un document ‘.gcode ’.
Pour produire un tel fichier depuis fusion 360, il faut passer par un autre logiciel qui est le ‘PrusaSlicer’ (disponible en open source).

Donc pour ce faire in faut passer par 5 étapes.

-Enregistrer vos fichier 3d de fusion 360 en  ‘.stl ’

-Les ouvrir sur PrusaSlicer

-Effectuer les réglages d’impression 

-Générer un ‘.gcode’

-Le transmettre à l'imprimante 3d

# 2. Reglage de l'impression ( PrusaSlicer)

# IMPORTANT :
Pour commencer selectionner votre imprimante 3D, Faite attention à bien préciser la taille de la buse installée sur votre imprimante.


# (A) Au debut, placer ces pièces sur le plateau

- Pour commencer vous importer vos fichier et vous devez placer vos pièces sur le pateau de façon intelligente par rapport à la machine.
- La fonction de base pour placé sa pièce est celle illustreé en dessous, on selectionne la face de notre objet qui sera en contact avec le plateau.
 

![](../images/classic.jpg)

# (B) Et maintenant, les supports

La, cette étape est nécessaire seulement si votre objet à une forme complexe pour la machine.
Les support, il sont généré par le logiciel automatiquement, mais en se mettant en mode expert on peut les modifier, voici comment vous pouvez modifier les supports.


Les support sont liés à un problème de surplombs de votre objet mais une imprimantes 3d peu ce déplacé en porte-a-faux jusqu’a 40/45 degrés. 

Donc vous pouvez réduire les supports à imprimer comme on le vois sur l’illustration avec une fois le réglage de base à 55 et ensuite à 15.
![](../images/supportok.jpg)

Maintenant  la dimension des supports est aussi quelque chose de modifiable, pratique pour créer des supports plus grossiers permettant d’être retiré plus facilement après.
La voici une illustration vous montrant la différence entre les réglages de base sur la dimension des supports.
Et en dessous mes réglages d’impression plus grossier

![](../images/bordure.jpg)


# (C) On vérifie et c'est partie 
On verifie que tout nos reglage soit adaptés à la machine et on peut lancé nos fichier, une fois la decoupe généré vous avez une indication sur la durée que cela va prendre. 
Et voila un aperçu de mes deux fichiers.
![](../images/pret.jpg)

# 3. On laisse faire la machine

Et voila le fichier est lancé , donc on voit les impressions sur les deux machines.
La on est déja un peu avancé dans l'impréssion sur ces photos et on voit que pour l'instant tout s'imprime correctement


Les pièces en oranges compose les élement bas de la lampe et la grille  
Les pièces en vert composent la partie supérieur de la lampe
![](../images/prusa1.jpg)

# 4. Resultat obtenue 

L’impression de mes pièces c’est plutôt bien passée.
J’ai tout de suite voulu tester les filetages et ça a bien fonctionné après une peu de nettoyage sur les pièces.

Pour tout ce qui est des supports,
Les supports extérieurs sont bien partie comme le montre la photo si dessous et pour ceux à l’intérieur ça a été plus difficile d’avoir des paroles lisse mais cela est moins grave vue q’on ne le voit pas.

![](../images/accroche.jpg)

# 5. Assemblage

# La tension
Pour l’histoire du la tension créée dans la lampe, j’ai opté pour un tendeur, un objet facile à se procurer.

Tout simple, coupez le tendeur en fonction de la dimension que vous souhaitez et replié le bout puis serré le avec un colson à fin d’effectuer un noeud, comme sur cette image avec une partie à gauche raccourci.

![](../images/tendeur.jpg)

# Nous voilà à la fin.

Voici les étapes dans l’ordre d’assemblage.

1 Passer le file électrique 

2 Placer l’élastique tendue dans les trois pièces du milieu

3 Lester la pièce du socle pour plus de stabilité ( pas obligatoire, fonctionne très bien sans )

4 Visser les pièces au extrémités 

Voila une illustration.


![](../images/lampedetail.jpg)

# Pour les curieux des images de la lampe finie

![](../images/rendulampe.jpg)






















