# Projet final


# Objet final

Voici mon objet final ou plutôt mes modules d'impressions finaux. Je me suis servi de tubes PVC pour la création de ces deux lampes. La première approche fut de trouver un système de liaisons simples, modulables et économiques à réaliser. Par la suite est venue la question du socle, de l’articulation, la source lumineuse et la mise en place du système électrique. Toutes ces recherches m’ont amené à l’extension de tube PVC en lampe modulable avec des impressions 3D simples et peu onéreuses.
Dans la plomberie, il existe tout type de raccord comme les coudes 45 degrés, 30°, 90°, les réductions... mais justement les réductions sont des élements peu adaptés pour réaliser une lampe car ils ont pour rôle d'assurer un bon écoulement de l'eau donc j'ai voulu mettre en place un système plus esthétique.

Voici mes deux propositions de lampe, comme vous l'aurez compris, chacun peut réaliser sa lampe en fonction de ses goûts et des disponibilités concernant les tubes.

J'ai fait le choix d'imprimer mes pièces avec des couleurs vives qui marquent l'objet pour que l'intervention sur les lampes soit marquée.


**Lampe 4050**

Cette lampe se nomme ainsi car elle est composée de tube PVC 40mm et 50mm.

![This is another caption](images/4050.jpg)

**Lampe 4080**

Cette lampe se nomme ainsi car elle est composée de tube PVC 40mm et 80mm.

![This is another caption](images/4080.jpg)


![This is another caption](images/A.jpg)

Pour les impressions 3D, il existe quatres types de jonction: vous trouverez les fichiers Stl de ces jonctions ainsi que plusieurs autres jonctions pour que chacun puisse moduler sa propre lampe avec des tubes PVC de tailles différentes.


Voici un assortiment de jonctions en fonction du diamètre des tubes. 
**Attention** si votre tube fait 80mm à l'extérieur, choisissez un ficher avec inscrit '80mm' mais si vous voulez créer une jonction avec un tube PVC qui s'emboîte sur un tube de 80mm (par exemple un coude) choisissez '80mm int'

| Socle | jonction | douille | Grille |
| ------ | ------ | ------ | ------ |
| [Socle 80mm int](docs/images/Socle_80mm_int.stl) | [Jonction 80mm int 50mm int](docs/images/Jonction_80mm_int_50mm_int.stl) | [Douille 50mm int 80mm](docs/images/Douille_50mm_int_80mm.stl) | [Grille 80mm int](docs/images/Grille_80mm_int.stl) |
| [Socle 80mm](docs/images/ Socle_80mm.stl) | [Jonction 80mm int 50mm](docs/images/Jonction_80mm_int_50mm.stl) |[Douille 50mm 80mm](docs/images/Douille_50mm_80mm.stl) | [Grille 50mm target](docs/images/Grille_50mm_target.stl) |
| [Socle 50mm int](docs/images/ Socle_50mm_int.stl) | [Jonction 80mm int 40mm int](docs/images/Jonction_80mm_int_40mm_int.stl) |[Douille 40mm int 80mm int](docs/images/Douille_40mm_int_80mm_int.stl) |  |
| [Socle 50mm](docs/images/ Socle_50mm.stl) | [Jonction 80mm int 40mm](docs/images/Jonction_80mm_int_40m.stl) |[Douille 40mm int 50mm int](docs/images/Douille_40mm_int_50mm_int.stl) |  |
| [Socle 40mm int](docs/images/ Socle_40mm_int.stl) | [Jonction 80mm 50mm int](docs/images/Jonction_80mm_50mm_int.stl) |[Douille 40mm int 50mm](docs/images/Douille_40mm_int_50mm.stl) |  |
| [Socle 40mm](docs/images/ Socle_40mm.stl) | [Jonction 80mm 50mm](docs/images/Jonction_80mm_50mm.stl) |[Douille 40mm int 80mm](docs/images/Douille_40mm_int_80mm.stl) |  |
|  | [Jonction 80mm 40mm int](docs/images/Jonction_80mm_40mm_int.stl) |[Douille 40mm 80mm](docs/images/Douille_40mm_80mm.stl) |  |
|  | [Jonction 80mm 40mm](docs/images/Jonction_80mm_40mm.stl) |[Douille 40mm 50mm int](docs/images/Douille_40mm_50mm_int.stl) |  |
|  | [Jonction 50mm int 40mm int](docs/images/Jonction_50mm_int_40_int.stl) | |  |
|  | [Jonction 50mm int 40mm](docs/images/Jonction_50mm_int_40mm.stl) | |  |
|  | [Jonction 50mm 40mm int](docs/images/Jonction_50mm__40mm_int.stl) | |  |
|  | [Jonction 50mm 40mm](docs/images/Jonction_50mm_40mm.stl) | |  |

Voici un exemple des fichiers utilisés pour réaliser la lampe 4080.
![This is another caption](images/B.jpg)




# Recherches
**L'axe de travail du projet**

Comme cité précédemment dans ma présentation,
J’ai sélectionné cet objet dans une collection d’objet en plastique et maintenant est venu le temps de réaliser un projet en lien avec cet objet.


![This is another caption](images/lampe_design.jpg)




**4025, OLAF VON BOHR**


**1973, ABS, mixed media **
**Kartell ITA**

Pour ce faire j’ai cinq chemins qui s’offrent à moi, ces chemins sont dictés par un mot qui servira de ligne directrice à mon projet.

Le premier proposé est **Référence** qui est pour moi un terme un peu trop vaste pour une ligne directrice de projet donc je n’ai pas pris ce chemin.

Le second est **Influence**, après en avoir bien lu la définition, pour moi ce terme fait que je serai trop sous l’emprise de la lampe 4025 et pas assez libre de mes propres choix durant le projet.

Le troisième est **Hommage**, il m’a tout d’abord fait penser au terme précédent avec sa référence historique ‘Acte, serment du vassal qui se déclarait l'homme de son seigneur’ mais en lisant plus en détail j’ai aussi retenu qu’un hommage est aussi une façon de témoigner son respect à l’objet.

le quatrième est **Inspiration**, celui-ci est un peu comme le premier référence, un terme pas assez puissant, l’inspiration pour moi s’est plus traduit comme un conseil lors de la lecture de sa définition.



Et pour le nom de ce projet, étant donné que la référence s'appelle lampe '4025' et que je la réinterpréte dans une plus grande dimension, son nom sera '40250'

Et le cinquième et dernier, l’**Extension**, un terme très intéressant et qui me semble bien coller avec mon projet et son avancement, maintenant que j’ai réalisé une réplique de la lampe 4025, l’extension semble être le bon chemin, le travail sur l’échelle de l’objet, les termes comme accroissement, élargissement, propagation, agrandissement sont des termes présents dans la définition de extension et sont des termes forts.

Donc après réflexion mon mot sera Extension mais le choix fut compliqué avec hommage donc créer une **extension dans le respect de l’objet** sera ma ligne directrice pour ce projet.



## Le Projet



Donc voilà maintenant il est temps de se jeter dans le bain de l’extension, mais plein d’idées me viennent en tête…

-Une réalisation à plus grande échelle

-Une réalisation réutilisant le système d’articulation sur une plus grande hauteur

-Une seconde fonction à l’objet

-Filtrer son éclairage avec un ajout lui permettant de remplir sa fonction de lampe de différentes façons

Et plein d’autres idées.

Me voilà aujourd’hui dans la réflexion sur ce projet.


## Le Projet c'est partie


Pour le projet, après avoir réalisé l’objet, j’en voulais plus.
Tout d’abord j’ai pensé à rajouter une rotule à l’objet mais je trouverai ça un peu simple et peu logique car l’objet est déjà orientable dans quasiment toutes les directions.

Mais l’idée de créer l’objet en plus grand me semble être un beau projet d’extension.
Pour ce faire, je suis reparti d’une vieille pensée qui était de réutiliser des tuyaux pvc pour réaliser les cylindres de la lampe.

J’ai choisi d’utiliser des tubes PVC de 12cm de diamètre intérieur.
Pour trois raisons.

- Premièrement, je trouvais que la dimension était plus importante et pas non plus trop importante pour une simple lampe

- Deuxièmement, j’ai trouvé du tube PVC de cette dimension donc pas besoin d’en acheter ( réemploi )

- Troisièmement, au dessus de 12cm, il est plus compliqué pour un particulier de trouver du tube PVC. Donc je trouvais la dimension vraiment bien choisie.


![This is another caption](images/exf1.jpg)

Maintenant que le choix est fait il est temps de modéliser les pièces complémentaires de la lampe sur fusion 360.

Donc j’ai décidé de recommencer mes modélisations et non de changer l’échelle de mes modélisations de la lampe à échelle 1:1 .
Pour différentes raisons, ne pas me tromper sur l’échelle mais aussi pour bien comprendre ce que je fais et adapter correctement mes raccords avec les tubes en PVC.

Par rapport au système de tension à l’intérieur de la lampe, je suis parti sur le même principe que pour la lampe à échelle 1:1 mais en laissant une réservation pour deux tendeurs au cas ou un seul ne suffirait pas pour bien articuler cette nouvelle grande lampe.

Donc voici mes pièces, entre chacune d’elle nous aurions les tubes PVC


![This is another caption](images/objet2.jpg)

Maintenant que j’ai réalisé mes pièces, il me manque encore la grille venant sur la tête de la lampe pour créer des jeux de lumière.

Pour cette grille j’ai décidé de rendre hommage à l’objet à échelle 1:1 avec une grille de plus grande dimension certes mais en gardant l’espacement des barreaux. 
Cela sert de rappel par rapport à l’objet original.

Vous pouvez voir sur l’image de gauche la grille pour la nouvelle grande lampe et à droite la grille de la lampe à échelle 1:1.


![This is another caption](images/grillediff.jpg)




Après tout cela me voilà au moment de réaliser mes pièces, dont la plus grande la tête.
Vérification que l’objet à grande échelle est imprimable sur le plateau de 20/25cm de la Prusa mk3.
Et la, on remarque qu’on atteint les limites d’impressions de la machine, à 1 ou 2cm prêt.
C’est à ce moment-là que je me suis dit que le tube PVC de 12cm de diamètre est un très bon choix.

Voici les impressions. On peut voir que la pièce de la tête en orange prend quasiment toute la largeur du plateau.
Et à coté la grille de celle-ci

![This is another caption](images/prusa.jpg)

Voici une première réalisation de prototype.

![This is another caption](images/aaa.jpg)

Après réflexion de ma part et conseil de collègues et professeurs, je me suis rendu compte que je n’étais pas satisfait de ce premier prototype pour différentes raisons :

- Le travail sur les couleurs n’est pas encore bien réglé, elles ne se marient pas très bien pour des raisons d’influences l’une sur l’autre, ou dans ce cas précis le marron prend trop d’ampleur.


- Les articulations ne sont pas assez performantes et ne permettent pas d’utiliser correctement l’objet.
Ce problème est en lien avec les couleurs, les articulations étant trop tassées, elles ne prennent pas bien leurs rôles.


- L’élément supérieur est trop proche de l’objet de référence ‘lampe 4025’ , même si le choix de l’axe de ce projet est fait autour de l’extension de l’objet et non la réinterprétation totale, je pense qu’il serait intéressant de travailler sur une autre forme pour cet élément,


# Je rebondis


## L'articulation 
Après ce premier prototype, il me fallait d’abord régler le problème de l’articulation.

J’aurais pu augmenter la dimension de ma sphère tout simplement, mais dans une idée de proportion correcte à mes yeux celle-ci devait être du même diamètre que mes tubes.

Je me suis donc dirigé vers une bague emboitée dans le tube PVC pour être maître de son épaisseur intérieure.

Dans l’image ci-dessous voici à gauche l’articulation du premier prototype, et à droite la nouvelle articulation avec cette bague comme plugging.

Celle-ci est beaucoup plus esthétique et fonctionnelle. On voit que la rotation est plus importante et que l’objet fera moins tassé par la suite.



![This is another caption](images/rectif.jpg)


Maintenant que ce problème est réglé pour moi, la suite 

## Les couleurs

Tout d’abord il fallait que j’exprime comment je voyais les couleurs de mon objet une fois fini. 

Pour ce faire je n’ai pas choisi le orange bêtement parce que la bobine de filament placé sur la machine était de cette couleur.
Mais tout d’abord parce que c’est une couleur que j’apprécie beaucoup et très utilisée dans la réalisation d’objet en plastique.

Ensuite le choix du tube PVC marron s’est fait naturellement car je le trouvais beau et mariable avec le orange ( je n’aurai pas utilisé de tube PVC si je n'avais trouvé que du gris )

Ensuite ces couleurs et l’époque de mon objet de référence m’ont fait penser à ces fameux papiers-peint. 

Donc je vois mon objet fini avec des couleurs faisant référence au papier peint des images ci-dessous.


![This is another caption](images/couleur.jpg)

Bon, maintenant que je sais ou je veux aller avec mes couleurs, il est tant de marier mon orange, marron et jaune de façon la plus harmonieuse .

Pour commencer je trouvais que le marron prenait une trop grande place donc je devais revoir la dimension de mes tubes et en avoir de plusieurs dimensions pour essayer.

Voici une image des différentes pièces pour mes tests et maintenant, plus qu’a présenter devant les yeux différents assemblages pour voir ce qui est le plus intéressant. 

![This is another caption](images/batterietest.jpg)


Voici  ci-dessous, plusieurs tests pour vous donner une idée de mon choix par rapport à un autre (très personnelle) 

Mais le fait d’avoir revu mon articulation avec une bague au niveau du tuyaux je trouve que l’articulation est mieux définie grâce à sa couleur, le fait que l’on voit plus la sphère, et que ce n’est plus un tube marron qui glisse maladroitement sur une sphère jaune ou orange mais une articulation jaune ou orange dans laquelle vient s’emboiter un tube, cela change beaucoup.


![This is another caption](images/testh.jpg)

Voici mon choix, maintenant je suis content du résultat et le trouve bien plus esthétique que précédemment. 

![This is another caption](images/1erjury.jpg)

## La tête

Maintenant que je trouve mon objet à grande échelle plus séduisant de par ses articulations fonctionnelles et la place que prennent chaque couleur.

Je suis retourné voir l’objet de référence (lampe 4025) pour trouver une nouvelle forme à l’élément supérieur de la lampe.

Ma démarche pour en tirer de nouvelles idées était de me questionner sur ce à quoi les formes de la lampe me fesaient penser.

Et j’ai trouvé que l’élément de l’articulation ( élément fondamental de ce projet ) était un volume dans les formes me font penser à un isolateur électrique ( ancien type d’isolateur ) et j’ai trouvé ça très intéressant pour mon projet.
Etant un fan du design rétro-futuriste, le travail avec ce genre d’élément me permettrait d’avoir un projet rentrant dans ce style design.

Voici donc une illustration de la lampe 4025 mise en relation avec mes prototypes d’articulations et ces fameux isolateurs électriques, pour vous faire comprendre correctement mes pensées
![This is another caption](images/iso1.jpg)

Ci-dessous un isolateur électrique de première génération, jouant son rôle pour bien en comprendre le fonctionnement primaire.

![This is another caption](images/iso2.jpg)

Maintenant que j’ai trouvé l’inspiration pour modifier l’élément supérieur de ma lampe, je me suis dis quel pourrait être sa forme?

Travaillant avec le mot extension, pour moi la suite logique est de travailler avec un tête ayant la forme d’un isolateur électrique de nouvelle génération.
Comme une extension du passée.

Voici, ci-dessous à quoi ressemblent ces isolateurs électriques de nos jours.
Ce sont des éléments prenant la forme d’une succession de disque.
Une forme séduisante pour ma part et pouvant jouer le rôle de l’élément supérieur de ma lampe.


![This is another caption](images/iso3.jpg)

J’ai fait un premier test de réalisation de cet élement supérieur en forme d’isolateur électrique et voici mes premières modélisations sur Fusion360.

Le but étant de ne pas à avoir à remonter le système d’articulation à chaque fois que j’essayerais une nouvelle tête, j’ai créé un première élément (à droite de l’image ci-dessous) avec un pas-de-vis ou l’on n’aurait plus qu’à visser la nouvelle tête. 

Ceci dit ce système était dans trouvais les cas obligatoire pour avoir un objet fonctionnel, en partant du principe qu’une ampoule à une durée de vie limitée, il faudrait que l’utilisateur puisse facilement la changer.




![This is another caption](images/fufu.jpg)

L’image en dessous est ma première réalisation mise en lien avec un isolateur électrique.

Ma réalisation ne s’est pas très bien déroulé, par ma faute, j’ai été trop ambitieux sur les bridges de la Prusa MK3 et donc je n’ai pas généré de support intentionnellement ( vu que je souhaite faire passer la lumière à travers l’objet, je souhaitais un objet des plus soigné et donc sans marque de support) et c’était une erreur de ma part. 
Je ferais en fonction pour mes prochaine tentatives.

De plus avec ce premier échec je peux me rendre compte que ma forme n’est pas encore correcte et donc à modifier en jouant sur la largeur et épaisseurs des disques.


![This is another caption](images/iso4.jpg)

Après cela, j'ai quand même pris soin d'essayer de mettre une source lumineuse dans l'objet pour en voir sa transparence.

![This is another caption](images/testnew.jpg)

Maintenant la suite avant d’arriver au prototype final.


# Je rebondis


Après avoir eu une discussion sur le projet, j’ai du remettre en question mes références pour des raisons bien justifiables.

Les points à revoir sont les couleurs, le socle puis le principal : la tête.



## Le socle 

Pour le socle j’ai réalisé jusque là un socle avec les mêmes dimensions de cylindre que le corps alors que la lampe 4025, elle, a un socle plus large, ce qui permet une meilleur stabilité mais aussi rend l’objet plus séduisant au niveau de ses formes. 

Donc pour ce faire j’aimerais créer un socle de plus grande dimension tout simplement.

## Les couleurs

Et par rapport au travail sur les couleurs, je pense faire plusieurs essais début janvier à l’impression 3d pour proposer différentes têtes de l’objet et donc à ce moment la je pourrais revoir mes choix de couleurs.


## La tête

La raison principale est celle liée à la tête de l’objet et sa fonction.
Cette lampe est une lampe orientable de par ses articulations donc la lumière qu’elle créae ne peut pas être diffusée à 360 degrée sinon l’orienter devient inutile.
Donc l’objectif est de revoir la tête de l’objet avec cette donnée et d'en faire une force.

Pour cela je suis retourné voir l’objet de référence et me suis demandé comment revoir cette tête en pensant au mot ‘Extension’ , axe directeur de mon projet.
Et j’en suis arrivé à une extension de la diffusion de lumière et donc dupliquer la tête pour lui offrir deux directions.

Comme on peut le voir sur le dessin ci-dessous les propositions pour une extension de la tête.
Au dessus des deux esquisses, un schéma d’un bureau en plan avec des possibilités d’orientation de lumière.

![This is another caption](images/dessin.jpg)

Après avoir fait ces schémas, j’ai commencé à réaliser sur fusion 360 les deux têtes,
Voici les premières modélisations sur fusion 360.

![This is another caption](images/tetes.jpg)
## Détail de construction 

Pour réaliser mon futur objet il suffira d’imprimer en 3D les éléments comme les articulation, bague, socle et tête de l’objet dans les couleurs definies.
- Socle.stl            /               Socle.gcode
- Articulation du bas.stl      /          Articulation du bas.gcode
- Articulation du haut.stl      /          Articulation du haut.gcode
- Bagues.stl                          /            Bagues.gcode
- Tête.stl.                            /              Tête.gcode



Couper des tubes PVC marron de la bonne dimension, ou celle de votre choix ( je présenterais différentes options de dimensions au jury final)


Suivre la démarche d’assemblage avec le système électrique et de tension. ( Principe de montage disponible dans le module3 )
![This is another caption](images/dessin2.jpg)
Et le tour est joué.


## Conception et programme du projet

La conception de ce projet se fera à l’aide des imprimantes 3D essentiellement.
Une base de savoir sur les branchements électrique sera nécessaire mais fourni par des explications pour monter l’objet.

Et le programme que devra remplir l’objet sera celui d’une lampe orientable à 360 dégrées avec une lumière bidirectionnelle  et un abas-jour interchangeable .
Sans oublier que cette lampe sera aussi un élément décoratif donc esthétique.

## Attitude par rapport à l'extension

Par rapport au terme extension, je m’en suis servi tout au long de mon processus de réflexion.
Il est visible du fait que l’objet a changé de dimension et s’est donc agrandit mais aussi différemment dans le cas de la forme de la tête avec le principe de duplication, dédoublement.

Vu que l'objet de référence se nomme '4025' et que je le reinterprète dans une plus grande dimension, le nom du prototype final sera en fonction de sa taille et de la tête selectionnées.

## Lampe 8050

L'objectif est de créer la lampe 4025 avec des dimensions doublées et une tête à double orientation.

Voici un dessin technique représentant les dimensions du corp de la lampe 8050.

L'objectif est de le reprensenter avec des dimensions et épaisseurs correctes pour créer les modélisation 3D de bonne épisseurs.



![This is another caption](images/lampe_8050.jpg)

Voici des tests de couleur pour choisir le filament lors de l'impressions, je suis plus intérressé par la version tout à doite, orange et blanc crème

![This is another caption](images/couleurr.jpg)



# Je rebondis

Après avoir discuté avec l’équipe du Fablab, nous nous sommes rendu compte que mes recherches de prototype étaient bien trop proche de l’objet de référence.
Cela est un gros problème pour des raisons de plagiat de l’objet.

Donc nous nous sommes donné une nouvelle consigne qui est de travailler avec le tube PVC, qui est un élément fondateur de mon projet.

Pour se faire, j’ai commencé à m’intéresser à des lampes sur table pouvant m’inspirer.

J’en ai retenue deux qui m’ont intéressé pour leur aspect tubulaire qui sont: 

Celle de gauche ‘Periscopio’ un projet de Danilo et Corrado Aroldi pour Stilnovo. Un lampe de bureau de 1966.

Puis celle de droite ‘602’ un projet de Cini Boeri pour Arteluce. Une lampe de la même époque et donc de 1968.

![This is another caption](images/lampeitalienne.jpg)


Ensuite après coup, je me suis mis à faire des tests de forme en présentant différents tubes de différent angle, diamètre devant moi.

J’avais à disposition du tube de 40mm  50mm 80mm ce qui est déjà un bon ensemble de dimensions. 
Le tout sous forme de tube droit ou coudé de 45 ou 90 degrés.

L’objectif est pas de simplement proposer une nouvelle lampe mais un principe adaptable sur différentes tailles de tube PVC. Le but étant de proposer un système d’impression 3D pour transformer de simples tubes PVC en lampe en fonction des envies de l’utilisateur.


Pour ce faire je suis partie sur un principe de bague raccordant deux tubes, j’ai choisit la bague pour servir de ‘réduction’ qui est un élément de plomberie difficile à adapter pour une lampe et peu esthétique. Donc créer une bague jouant le rôle de réduction de façon séduisante me semblait être une bonne idée .

De même pour le travail de socle avec un bouchon pour le tube plus large permettant de créer un socle stable à la lampe.


Et pour la partie qui éclair de la lampe, pour des raisons d’économie d’impression j’ai arrêté de travailler sur une tête venant se relier à un tube car cela créait un gros élément à imprimer. J’ai essayé de simplement proposer des caches ampoules venant se fixer à l’extrémité du tube.


Voici des dessins des deux prototypes de lampe que j’ai voulu présenter avec ces systèmes d’assemblages.

Une repésentation du premier objet.

![This is another caption](images/lampe1_.jpg)

Sa coupe et ses pièces.

![This is another caption](images/detailL1.jpg)


Une repésentation du second objet .


![This is another caption](images/lampe2.jpg)

Sa coupe et ses pièces.

![This is another caption](images/detailampe2.jpg)



L’objectif pour moi serait de réaliser une de ces deux lampes pour la présenter au musée. Et montrer qu' avec du tube PVC, deux trois impressions 3d ingénieuses on peut arriver à un résultat intéressant.





